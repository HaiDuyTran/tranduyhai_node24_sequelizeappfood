const express = require("express");
const foodRoute = express.Router();

const { getFoodDemo } = require("../controllers/foodController");

// const upload = require("../middleware/upload");
// //, upload.single("upload")

// foodRoute.post("upload", upload.single(), uploadFood);

//
foodRoute.get("/demo", getFoodDemo);

module.exports = foodRoute;
